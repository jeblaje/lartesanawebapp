import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { ShopComponent } from './pages/shop/shop.component';
import { concat } from 'rxjs';
import { ContactComponent } from './pages/contact/contact.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'contact', component: ContactComponent },
  // {path: 'register', component: RegisterComponent},
  // {
  //     path: '',
  //     component: PagesComponent,
  //     canActivate: [LoginGuardGuard],
  //     loadChildren: './pages/pages.module#PagesModule'
  // },
  // {path: '**', component: NopagesfoundComponent},
];

export const APP_ROUTES = RouterModule.forRoot(appRoutes, { useHash: true });
